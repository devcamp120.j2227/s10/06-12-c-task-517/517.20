import { Component } from "react";

class Click extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0
        }

        // C1: Sử dụng bind để trỏ this về class
        // Hàm bind có tác dụng trỏ lại this cho 1 hàm
        // this.onButtonClickHandler = this.onButtonClickHandler.bind(this);
    }

    // C2: Arrow function không có this, nên this trong arrow function mặc định trỏ về class
    onButtonClickHandler = () => {
        console.log("Click here!");
        console.log(this);
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        return (
            <div style={{margin: "20px"}}>
                <p>Count click: {this.state.count}</p>

                <hr />

                <button onClick={this.onButtonClickHandler}>Click here</button>
            </div>
        )
    }
}

export default Click;